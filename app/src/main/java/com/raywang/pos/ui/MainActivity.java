package com.raywang.pos.ui;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.raywang.pos.R;
import com.raywang.pos.ui.fragment.GoodsReceiptFragment;
import com.raywang.pos.ui.fragment.MenuFragment;
import com.raywang.pos.ui.fragment.PayFragment;
import com.raywang.pos.ui.fragment.SaleFragment;
import com.raywang.pos.ui.fragment.SearchFragment;
import com.raywang.pos.util.UIHelp;
import com.raywang.pos.view.drawerlib.ActionBarDrawerToggle;
import com.raywang.pos.view.drawerlib.DrawerArrowDrawable;

/**
 * 主界面的activity
 * @author Ray
 *
 */
public class MainActivity extends BaseActivity   {
	/** 抽屉控件*/
	private DrawerLayout drawerLayout;
	/** 滑出来的菜单FrameLayout*/
	private View mDrawerMenu;
	/** 动画的2个组件*/
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerArrowDrawable drawerArrow;
	
	/** 左滑菜单的Fragment*/
	private MenuFragment menuFragment;

	private long time;

    private SaleFragment saleFragment;
    /** 是否是显示的键盘输入界面*/
    private boolean showKey = true;
    private ImageView keyOrTouch;


	private MenuFragment.OnMenuClick menuClick = new MenuFragment.OnMenuClick(){
		@Override
		public void onMenuClick(int index) {
			if(fragments[index] == null) {
				switch (index) {

					case 1:

						GoodsReceiptFragment goodsReceipt = new GoodsReceiptFragment();
						fragments[index] = goodsReceipt;
						break;
					case 2:
						if(fragments[index] == null){
							SearchFragment searchFragment = new SearchFragment();
							fragments[index] = searchFragment;
						}

						break;
					default:
						break;
				}
			}
			if(fragments[index] != null)
				replace(R.id.mainContent, fragments[index]);
			drawerLayout.closeDrawer(mDrawerMenu);
			menuFragment.onMenuClick(index);
		}
	};
	
	/** 每个菜单对应的Fragment*/
	private Fragment[] fragments = null;
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_main);
	}
	
	protected void iniFragment() {
		iniBar();
		//抽屉菜单的Fragment
		menuFragment = new MenuFragment();
		
		menuFragment.setMenuClick(menuClick);
		add(R.id.leftMenu, menuFragment);

		fragments = new Fragment[getResources().getStringArray(R.array.menu_array).length];
		saleFragment = new SaleFragment();
        saleFragment.setListener(this);
		fragments[0] = saleFragment;
		add(R.id.mainContent, fragments[0]);
		
		//

		super.iniFragment();
	}
	
	

	
	/**
	 * 初始化标题部分Bar
	 */
	private void iniBar(){
		//设置bar为自定义的bar
		ActionBar bar = getActionBar();
		bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		bar.setCustomView(R.layout.head);

		//设置bar上面的按钮的监听
		View view = bar.getCustomView();
		view.findViewById(R.id.img).setOnClickListener(this);
		view.findViewById(android.R.id.home).setOnClickListener(this);
        keyOrTouch = (ImageView) view.findViewById(R.id.keyortouch);
        keyOrTouch.setOnClickListener(this);
		
		
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		
		mDrawerMenu = findViewById(R.id.leftMenu);
		
		//主要设置图片旋转的动画
		drawerArrow = new DrawerArrowDrawable(this) {
			
			public boolean isLayoutRtl() {
				return false;
			}
		};
		mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				drawerArrow, R.string.drawer_open, R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				invalidateOptionsMenu();
			}
			
			
		};
		drawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
	}
	
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	protected void click(int id) {
		switch (id) {
            case android.R.id.home:
            case R.id.img:
                if (drawerLayout.isDrawerOpen(mDrawerMenu)) {
                    drawerLayout.closeDrawer(mDrawerMenu);
                } else {
                    drawerLayout.openDrawer(mDrawerMenu);
                }
                break;
    //		case R.id.search:
    //			menuClick.onMenuClick(2);
    //			break;
            case R.id.pay:
                if(saleFragment.canPay()) {
                    PayFragment payFragment = new PayFragment();
                    payFragment.setGoods(saleFragment.getGoodsApdater());
                    payFragment.setSaleFragment(saleFragment);
                    replaceToBack(R.id.mainContent, payFragment);
                }
                break;
            case R.id.keyortouch:
                if(showKey){
                    keyOrTouch.setImageResource(R.drawable.iconfont_keyboard);
                }else{
                    keyOrTouch.setImageResource(R.drawable.iconfont_touch);
                }
                showKey = !showKey;
                if(saleFragment != null){
                    saleFragment.changeKeyOrTouch(showKey);
                }
                break;
            default:
                super.click(id);
                break;
	}
	}



	@Override
	public void onBackPressed() {
		if(getFragmentManager().getBackStackEntryCount() == 0) {
			if (System.currentTimeMillis() - time < 2000) {
				finish();
				return;
			}
			time = System.currentTimeMillis();
			UIHelp.toast(this, "再次按back键退出");
			return;
		}
		super.onBackPressed();
	}


}
