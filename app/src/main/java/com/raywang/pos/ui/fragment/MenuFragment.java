package com.raywang.pos.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.raywang.pos.R;
import com.raywang.pos.ui.adapter.MenuAdapter;

import java.util.AbstractCollection;

/**
 * 菜单的fragment
 * @author Ray
 * @date 2015年6月26日09:51:13
 * @version 1.0
 */
public class MenuFragment extends BaseFragment {

	private ListView menus;
	/** 菜单的点击监听*/
	private OnMenuClick menuClick;
	
	private MenuAdapter adapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_menu,container,false);
		iniView(view);
		return view;
	}
	
	protected void iniView(View view) {
		menus = (ListView) view.findViewById(R.id.menus);
		String[] array = getResources().getStringArray(R.array.menu_array);
		adapter = new MenuAdapter(getActivity(), array);
		menus.setAdapter(adapter);
		
		menus.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				if(menuClick != null){
					menuClick.onMenuClick(arg2);
				}
			}
		});
		super.iniView(view);
	}

	public void setMenuClick(OnMenuClick menuClick) {
		this.menuClick = menuClick;
	}





	/**
	 * 菜单点击的监听
	 * @author Ray
	 * @date 2015年6月26日10:58:32
	 */
	public interface OnMenuClick{
		/**
		 * 菜单被点击了
		 * @param index 菜单的索引
		 */
		public void onMenuClick(int index);
	}

	public void onMenuClick(int index){
		adapter.setSelected(index);
	}
}
