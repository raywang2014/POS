package com.raywang.pos.ui.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.raywang.pos.R;
import com.raywang.pos.bean.Goods;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * 准备销售的商品的数据适配器
 * Created by Ray Wang on 2015/7/4.
 */
public class GoodsAdapter extends BaseAdapter {
    private ArrayList<Goods> datas;
    private LayoutInflater inflater;

    public GoodsAdapter(Context context,ArrayList<Goods> datas){
        this.inflater = LayoutInflater.from(context);
        this.datas = datas;
    }
    @Override
    public int getCount() {
        if(datas != null){
            return datas.size();
        }
        return 0;
    }

    @Override
    public Goods getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        if(convertView == null){
            view = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_goods,parent,false);
            view.allmoney = (TextView) convertView.findViewById(R.id.allmoney);
            view.count = (TextView) convertView.findViewById(R.id.count);
            view.name = (TextView) convertView.findViewById(R.id.name);
            view.price = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(view);
        }else{
            view = (ViewHolder) convertView.getTag();
        }

        Goods goods = datas.get(position);
        view.name.setText(goods.getName());
        view.price.setText("￥"+goods.getPrice());
        view.count.setText(""+goods.getCount());
        view.allmoney.setText("￥"+(goods.getPrice()*goods.getCount()));

        return convertView;
    }

    public void addGoods(Goods goods){
        for(int i = 0;i < this.datas.size();i++){
            if(datas.get(i).getCode().equals(goods.getCode())){
                datas.get(i).setCount(datas.get(i).getCount()+1);
                notifyDataSetChanged();
                return;
            }
        }
        goods.setCount(1);
        this.datas.add(goods);
        notifyDataSetChanged();
    }

    /**
     * 删除商品
     * @param index
     */
    public void deleteGoods(int index){
        if(index < datas.size()){
            datas.remove(index);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder{
        TextView name;
        TextView price;
        TextView count;
        TextView allmoney;
    }

    public void clean(){
        this.datas.clear();
        notifyDataSetChanged();
    }


    /**
     * 获取销售总额
     * @return
     */
    public double getMoney(){
        if(this.datas.size() > 0){
            double money = 0;
            for(Goods goods : datas){
                money += goods.getPrice() * goods.getCount();
            }
            return money;
        }
        return 0;
    }

    public void setGoods(Goods goods){
        for(int i = 0; i < datas.size();i++){
            if(datas.get(i).getCode().equals(goods.getCode())){
                if(goods.getCount() == 0){
                    datas.remove(i);
                    break;
                }else{
                    Goods temp = datas.get(i);
                    temp.setCount(goods.getCount());
                    temp.setPrice(goods.getPrice());
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getAllCount(){
        if(this.datas.size() > 0){
            int count = 0;
            for(Goods goods : datas){
                count += goods.getCount();
            }
            return count;
        }
        return 0;
    }

    public ArrayList<Goods> getDatas(){
        return datas;
    }
}
