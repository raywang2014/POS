package com.raywang.pos.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.raywang.pos.R;
import com.raywang.pos.listener.OnLoginSuccess;
import com.raywang.pos.util.UIHelp;
import com.raywang.pos.util.Util;


/**
 * 登陆的Fragment
 * @author Ray
 *
 */
public class LoginFragment extends BaseFragment {

	private OnLoginSuccess loginSuccess;
	
	private EditText ename;
	private EditText epass;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, container,false);
		iniView(view);
		return view;
	}
	
	protected void iniView(View view) {
		view.findViewById(R.id.login).setOnClickListener(this);
		ename = (EditText) view.findViewById(R.id.username);
		epass = (EditText) view.findViewById(R.id.userpass);
		
		epass.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP){
                    login();
				}
				return false;
			}
		});
		super.iniView(view);
	}

	public void setLoginSuccess(OnLoginSuccess loginSuccess) {
		this.loginSuccess = loginSuccess;
	}
	
	protected void click(int id) {
		switch (id) {
			case R.id.login:

                login();
				break;
	
			default:
				super.click(id);
				break;
		}
	}
	
	/**
	 * 登陆的方法
	 */
	protected void login() {

			String name = ename.getText().toString();
			String pass = epass.getText().toString();
			if ("".equals(name)) {
				UIHelp.toast(getActivity(), "用户名不能为空");
				return;
			}
			if ("".equals(pass)) {
				UIHelp.toast(getActivity(), "密码不能为空");
				return;
			}
			if ("admin".equals(name) && "admin".equals(pass)) {
				if (loginSuccess != null) {
					loginSuccess.onLoginSuccess();
				}
			} else {
				UIHelp.toast(getActivity(), "用户名或密码错误");
			}
	}
}
