package com.raywang.pos.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raywang.pos.R;
import com.raywang.pos.listener.OnLoadingFinish;


/**
 * 打开软件的过度画面的Fragment
 * @author Ray
 */
@SuppressLint("HandlerLeak")
public class LoadingFragment extends BaseFragment {
	
	private OnLoadingFinish finish;
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(finish != null){
				finish.onLoadingFinish();
			}
		};
	};

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_loading, container,false);
		handler.sendEmptyMessageDelayed(1, 3000);
		return view;
	}

	public void setFinish(OnLoadingFinish finish) {
		this.finish = finish;
	}

	
	
}
