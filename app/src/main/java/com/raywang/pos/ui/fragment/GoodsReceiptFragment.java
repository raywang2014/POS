package com.raywang.pos.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raywang.pos.R;

/**
 * 店铺收货的Fragment
 * @author Ray
 * @date 2015年6月26日11:19:52
 */
public class GoodsReceiptFragment extends BaseFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_goodsreceipt,container,false);
		return view;
	}
}
