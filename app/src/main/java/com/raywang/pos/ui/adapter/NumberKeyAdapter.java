package com.raywang.pos.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.raywang.pos.R;

/**
 * 按键的数据适配器
 * Created by Ray on 2015/7/9.
 */
public class NumberKeyAdapter extends BaseAdapter{

    private String[] keys = new String[]{"1","2","3","4","5","6","7","8","9","0","00","."};

    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public NumberKeyAdapter(Context context,View.OnClickListener listener){
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }
    @Override
    public int getCount() {
        return keys.length;
    }

    @Override
    public Object getItem(int position) {
        return keys[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Button button = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_key_btn,parent,false);
            button = (Button) convertView.findViewById(R.id.key);
            convertView.setTag(button);
        }else{
            button = (Button) convertView.getTag();
        }
        button.setText(keys[position]);

        convertView.setId(position);
        convertView.setOnClickListener(listener);
        return convertView;
    }
}
