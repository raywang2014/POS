package com.raywang.pos.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.raywang.pos.R;

/**
 * 菜单的数据适配器
 * @author Ray
 * @date 2015年6月26日09:57:49
 * @version 1.0
 */
public class MenuAdapter extends BaseAdapter {

	private String[] menus;
	
	private LayoutInflater inflater;
	
	private int index = 0;
	
	private Context context;
	
	public MenuAdapter(Context context,String[] menus){
		this.inflater = LayoutInflater.from(context);
		this.menus = menus;
		this.context = context;
	}
	
	public int getCount() {
		if(menus != null){
			return menus.length;
		}
		return 0;
	}

	public Object getItem(int arg0) {
		return null;
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent){
		TextView view;
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.item_menu, parent,false);
			view = (TextView) convertView.findViewById(R.id.menu);
			convertView.setTag(view);
		}else{
			view = (TextView) convertView.getTag();
		}
		
		view.setText(menus[position]);
		
		if(index == position){
			view.setBackgroundColor(context.getResources().getColor(R.color.menu_selected));
		}else{
			view.setBackgroundColor(Color.WHITE);
		}
		
		return convertView;
	}

	public void setSelected(int index){
		this.index = index;
		notifyDataSetChanged();
	}


}
