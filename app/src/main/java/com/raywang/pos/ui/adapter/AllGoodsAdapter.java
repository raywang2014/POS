package com.raywang.pos.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.raywang.pos.R;
import com.raywang.pos.bean.Goods;

import java.util.ArrayList;

/**
 * 所有商品的数据适配器
 * 当前没有分类（下一个版本做分类）
 * Created by Ray on 2015/7/14.
 */
public class AllGoodsAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    private ArrayList<Goods> datas;

    public AllGoodsAdapter(Context context,ArrayList<Goods> datas){
        this.inflater = LayoutInflater.from(context);
        this.datas = datas;
    }

    @Override
    public int getCount() {
        if(datas != null){
            return datas.size();
        }
        return 0;
    }

    @Override
    public Goods getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_all_goods,parent,false);
            view = new ViewHolder();
            view.name = (TextView) convertView.findViewById(R.id.name);
            view.price = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(view);
        }else{
            view = (ViewHolder) convertView.getTag();
        }

        view.name.setText(datas.get(position).getName());
        view.price.setText("￥"+datas.get(position).getPrice());

        return convertView;
    }

    private static class ViewHolder{
        TextView name;
        TextView price;
    }
}
