package com.raywang.pos.ui;

import android.content.Intent;
import android.os.Bundle;

import com.raywang.pos.R;
import com.raywang.pos.listener.OnLoadingFinish;
import com.raywang.pos.listener.OnLoginSuccess;
import com.raywang.pos.ui.fragment.LoadingFragment;
import com.raywang.pos.ui.fragment.LoginFragment;


/**
 * 登陆的activity
 * @author Ray
 *
 */
public class LoginActivity extends BaseActivity{

	/** 加载数据的Fragment*/
	private LoadingFragment loadingFragment;
	/** 登陆的Fragment*/
	private LoginFragment loginFragment;
	
	private OnLoadingFinish finish;
	
	private OnLoginSuccess loginSuccess;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.activity_fragment);
		
	}

	
	protected void iniFragment() {
		loadingFragment = new LoadingFragment();
		loginFragment = new LoginFragment();
		
		finish = new OnLoadingFinish() {
			
			public void onLoadingFinish() {
				replace(R.id.mainFragment, loginFragment);
			}
		};
		
		loginSuccess = new OnLoginSuccess() {
			
			public void onLoginSuccess() {
				Intent intent = new Intent(LoginActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		};
		
		loadingFragment.setFinish(finish);
		loginFragment.setLoginSuccess(loginSuccess);
		
		replace(R.id.mainFragment,loadingFragment);
		
		super.iniFragment();
	}
}
