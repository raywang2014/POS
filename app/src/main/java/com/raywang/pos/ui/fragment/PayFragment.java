package com.raywang.pos.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.raywang.pos.R;
import com.raywang.pos.bean.Goods;
import com.raywang.pos.db.DataBase;
import com.raywang.pos.ui.adapter.GoodsAdapter;
import com.raywang.pos.ui.adapter.NumberKeyAdapter;
import com.raywang.pos.util.UIHelp;
import com.raywang.pos.view.RayGridView;

import java.util.ArrayList;

/**
 * Created by Ray on 2015/7/9.
 */
public class PayFragment extends BaseFragment {

    private GoodsAdapter adapter;

    private ListView goods;

    private EditText allmoney;
    private EditText paymoney;
    private EditText givemoney;


    /** 模拟键盘的按键GridView*/
    private RayGridView gridView;
    /** 模拟键盘的按键适配器*/
    private NumberKeyAdapter keyAdapter;
    private boolean isOneInput = true;
    private double allMoney = 0;

    private Button pay;

    private boolean isFinish = false;

    private SaleFragment saleFragment;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay,container,false);
        iniView(view);
        return view;
    }

    @Override
    protected void iniView(View view) {
        view.findViewById(R.id.back).setOnClickListener(this);
        goods = (ListView) view.findViewById(R.id.goods1);
        goods.setAdapter(adapter);

        allmoney = (EditText) view.findViewById(R.id.allmoney);
        paymoney = (EditText) view.findViewById(R.id.paymoney);
        givemoney = (EditText) view.findViewById(R.id.givemoney);



        view.findViewById(R.id.del).setOnClickListener(this);
        pay = (Button) view.findViewById(R.id.ok);
        pay.setOnClickListener(this);

        allMoney = adapter.getMoney();
        allmoney.setText(""+allMoney);
        paymoney.setText(""+allMoney);
        givemoney.setText("0");

        gridView = (RayGridView) view.findViewById(R.id.numbers);

        keyAdapter = new NumberKeyAdapter(getActivity(),new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFinish){
                    return;
                }
                if(isOneInput){
                    paymoney.setText(keyAdapter.getItem(v.getId()).toString());
                    isOneInput = false;
                }else {
                    paymoney.setText(paymoney.getText().toString() + keyAdapter.getItem(v.getId()));
                }
                paymoney.setSelection(paymoney.getText().toString().length());

                sumMoney();
            }
        });
        gridView.setAdapter(keyAdapter);

        super.iniView(view);
    }

    @Override
    protected void click(int id) {
        switch (id){
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.del:
                if(isFinish){
                    pay.setText("确定");
                    isFinish = false;
                }else {
                    String str = paymoney.getText().toString();
                    if (str.length() >= 1) {
                        str = str.substring(0, str.length() - 1);
                    }
                    paymoney.setText(str);
                    paymoney.setSelection(str.length());
                    sumMoney();
                }
                break;
            case R.id.ok:
                if(isFinish){
                    UIHelp.toast(getActivity(),"成功");
                    new DataBase(getActivity()).insertSaleGoods(adapter.getDatas(),"admin");
                    adapter.clean();
                    getActivity().onBackPressed();
                }else {
                    isFinish = true;
                    pay.setText("完成");
                    saleFragment.onGoodsCountChage();
                }
                break;
            default:
                super.click(id);
                break;
        }
    }

    /**
     * 计算找零
     */
    private void sumMoney(){
        double money = 0;
        if(!paymoney.getText().toString().equals("")){
            money = Double.parseDouble(paymoney.getText().toString());
        }
        double temp = money - allMoney;
        if(temp < 0){
            temp = 0;
        }
        givemoney.setText(""+temp);
    }


    public void setGoods(GoodsAdapter adapter) {
        this.adapter = adapter;
    }

    public void setSaleFragment(SaleFragment saleFragment) {
        this.saleFragment = saleFragment;
    }
}
