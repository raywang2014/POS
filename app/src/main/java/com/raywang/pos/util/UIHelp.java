package com.raywang.pos.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * 界面相关的工具类
 * @author Ray
 * @version 1.0
 */
public class UIHelp {

	/**
	 * Toast 提示框
	 * @param context 程序上下文
	 * @param msg 消息内容
	 */
	public static void toast(Context context,String msg){
		if(context != null){
			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * Toast 提示框
	 * @param context 程序上下文
	 * @param resId 消息String资源id
	 */
	public static void toast(Context context,int resId){
		if(context != null){
			Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
		}
	}
	
	
	/**
	 * 将DIP转化成px
	 * @param dp 
	 * @param context
	 * @return
	 */
	public static int dp2px(int dp,Context context) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				context.getResources().getDisplayMetrics());
	}
	
	/**
     * 获取屏幕的宽度
     * @param context
     * @return
     */
    public static int getWidth(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    /**
     * 获取屏幕宽度
     * @param context
     * @return
     */
    public static int getHeight(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    /**
     * 获取屏幕信息
     * @param context
     * @return
     */
    public static DisplayMetrics getDisplayMetrics(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }
}
