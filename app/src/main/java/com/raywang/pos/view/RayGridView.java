package com.raywang.pos.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 有多高就显示多高的GridView
 * Created by Ray on 2015/7/9.
 */
public class RayGridView extends GridView{
    public RayGridView(Context context){
        super(context);
    }
    public RayGridView(Context context, AttributeSet attrs){
        super(context,attrs);
    }
    public RayGridView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context,attrs,defStyleAttr);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RayGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
