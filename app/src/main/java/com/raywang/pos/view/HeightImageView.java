package com.raywang.pos.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * 宽度和高度相同的ImageView
 * Created by Ray on 2015/7/23.
 */
public class HeightImageView extends ImageView{

    public HeightImageView(Context context) {
        super(context);
    }

    public HeightImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HeightImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}
