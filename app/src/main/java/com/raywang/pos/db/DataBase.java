package com.raywang.pos.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.raywang.pos.bean.Goods;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * 数据库操作
 * Created by Ray Wang on 2015/7/4.
 */
public class DataBase extends SQLiteOpenHelper{

    private final static String DBNAME = "data.db";
    private final static int VERSION = 1;

    public DataBase(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists goods(code text,number text,name text," +
                "price number(9,2),count int)");
        db.execSQL("create table if not exists saleMain(id varchar(20),money number(9,2)," +
                "saleName varchar(20))");
        db.execSQL("create table if not exists saleDetail(id integer primary key,mainid" +
                " varchar(20),goodscode text,price number(9,2),count integer)");

        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0001','1','方便面',4.5,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0002','2','辣条',5,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0003','3','老干妈',9,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0004','4','怡宝',2,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0005','5','炫迈',8,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0006','6','红双喜',10,1000)");
        db.execSQL("insert into goods(code,number,name,price,count) " +
                "values('0007','7','火腿肠',9,1000)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists goods");
        onCreate(db);
    }

    public Goods findByNumberOrCode(String key){
        Goods goods = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select code,number,name,price from goods" +
                " where code=? or number=?",new String[]{key,key});
        if(cursor.moveToNext()){
            goods = new Goods(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                    cursor.getDouble(3),1);
        }

        return goods;
    }

    public ArrayList<Goods> findAll(){
        ArrayList<Goods> list = new ArrayList<Goods>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select code,number,name,price from goods",null);
        while(cursor.moveToNext()){
            list.add( new Goods(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                    cursor.getDouble(3),1));
        }
        cursor.close();
        return list;
    }

    public void insertSaleGoods(ArrayList<Goods> goodses,String saleName){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        long id = System.currentTimeMillis();
        //计算出总价
        double allMoney = 0;
        for(Goods goods : goodses){
            allMoney += goods.getCount() * goods.getPrice();
            db.execSQL("insert into saleDetail(mainid,goodscode,price,count)" +
                    " values(?,?,?,?)",new Object[]{id,goods.getCode(),goods.getPrice(),
                    goods.getCount()});
        }
        db.execSQL("insert into saleMain(id,money,saleName) values(?,?,?)"
            ,new Object[]{id,allMoney,saleName});

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
