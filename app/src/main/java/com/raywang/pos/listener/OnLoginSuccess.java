package com.raywang.pos.listener;
/**
 * 登陆成功后的监听事件
 * @author Ray
 *
 */
public interface OnLoginSuccess {

	
	public void onLoginSuccess();
}
