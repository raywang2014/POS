package com.raywang.pos.listener;
/**
 * 打开程序时加载数据的加载完成后执行的监听事件
 * @author Ray
 *
 */
public interface OnLoadingFinish {

	
	public void onLoadingFinish();
}
