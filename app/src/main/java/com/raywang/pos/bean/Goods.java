package com.raywang.pos.bean;

/**
 * 商品实体
 * Created by Ray Wang on 2015/7/4.
 */
public class Goods {
    /** 商品条码*/
    private String code;
    /** 商品编码*/
    private String number;
    /** 商品名称*/
    private String name;
    /** 价格*/
    private double price;
    /** 库存*/
    private int count;

    private double oldPrice;

    public Goods() {
    }

    public Goods(String code, String number, String name, double price, int count) {
        this.code = code;
        this.number = number;
        this.name = name;
        this.price = price;
        this.count = count;
        this.oldPrice = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }
}
